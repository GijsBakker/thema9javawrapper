# SScClassifier
SScClassifier is used to classify SSc patients into LcSSc, DcSSc or Other, using antibody count data.

Version: 1.0  
Date: 14-11-2020   
Author: Gijs Bakker

# Usage
To use this program, call WrapperUI via the command line or run it in a IDEA that has a way to give user input. This 
program can be run with or without commandline arguments. 
Command line arguments:
```
-f <file>:          The file with unclassified patients that needs to be classified.
-l:                 Indicator for if the data is log2 transformed or not. Leave out when the data is already log2 transformed.
                    Add if the data is not log2 transformed.
-i <count data>:    A line of counts separated by commas. Example "-i 0,81,0.0,3.67,30,30.0". The order of antibodies should
                    be the same as the order of the list in "Input file requirements". 
-o <file>:          The desired location and file name of the ourput file. Default = Data/PredictedValues.csv
```
When -f and -i are not given the program will ask the user to give a file name and it will ask the user to specify if 
the data is log2 transformed or not.

# Input file requirements
The file given to this program should contain instances of SSc patients that need to be classified. Make sure the file
is a csv file following a standard csv format. The file should contain at least the following columns with these exact 
names:
* Scl.70_EL_SC
* Scl.70_DTEK_SSC
* PM.Scl100_DTEK_SSC
* PM.Scl75_DTEK_SSC
* RNA.pol.III_DTEK_SSC
* Pm.Scl_immcap_SSC

The data in these columns should be either a numeric representation of the antibody count data, or a log2 transformation
on these antibody counts.  
An example csv file "TestData.csv" can be found in the folder Data. This file contains all the necessary columns and an 
extra column "DONOR".

# Requirements
* Java SDK-14
* R version 3.4.4 
* rstudioapi: version: 1.0.153 (R package)
* foreign: version: 0.8-80 (R package)

# Installation
Simply download/ clone the repository, and the program should be ready to use.