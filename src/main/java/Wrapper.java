import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import weka.classifiers.meta.CostSensitiveClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;


public class Wrapper {
    private final String model = "Model/J48Model.model";
    private final String Rscript = "Rscript Rscript/DataTransformer.R";
    private final String ROutput = "Data/DataToTest.arff";
    private final String ErrorLines = "Data/MissingData.csv";
    private String finalOutput;

    public static void main(String[] args) {
        if (args.length >0){
            Wrapper wrapper = new Wrapper();
            wrapper.start(args);
        }
    }

    private void start(String[] input){
        try {
            finalOutput = input[2];
            callR(input);
            CostSensitiveClassifier classifier = loadClassifier();
            Instances dataToTest = loadArff(ROutput);
            Instances labled = classifyNewInstance(classifier, dataToTest);
            createOutputFile(finalOutput);
            System.out.println("If a line has a missing value it will be labeled as NA.");
            writeOutputFile(input[0], labled);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void createSingleInstanceFile(String countData){
        String tempData = "Data/SingleInstance.csv";
        createOutputFile(tempData);
        try{
            Path path = Paths.get(tempData);
            BufferedWriter writer = Files.newBufferedWriter(path);
            writer.write("\"Scl.70_EL_SC\",\"Scl.70_DTEK_SSC\",\"PM.Scl100_DTEK_SSC\",\"PM.Scl75_DTEK_SSC\"," +
                    "\"RNA.pol.III_DTEK_SSC\",\"Pm.Scl_immcap_SSC\"\n");
            writer.write(countData);
            writer.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Creates output file
     */
    private static void createOutputFile(String file){
        Path dataPath = Paths.get(file);
        if(!Files.exists(dataPath)) {
            try {
                Files.createFile(dataPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method should add a row to the given input file with the expected labels
     * @param inputFile: the name of the input file
     * @param labeled: The instances that are labeled
     */
    private void writeOutputFile(String inputFile, Instances labeled){
        // read inputFile
        Path pathIn = Paths.get(inputFile);
        Path pathOut = Paths.get(finalOutput);
        Path pathMissing = Paths.get(ErrorLines);

        try {
            BufferedReader reader = Files.newBufferedReader(pathIn);
            BufferedReader missing = Files.newBufferedReader(pathMissing);
            BufferedWriter writer = Files.newBufferedWriter(pathOut);
            String lineMissing;
            String line;
            int instanceNumber = -1;
            while ((line = reader.readLine()) != null){
                lineMissing = missing.readLine();
                // in header
                if (instanceNumber == -1){
                    writer.write(line + ",\"PredictedLabel\"" + "\n");
                    // should also skip header of missing file
                    instanceNumber ++;
                }else{
                    // should check if line is in missingData file
                    if (lineMissing.equals("TRUE")){
                        writer.write(line + ",NA" + "\n");
                        System.out.println("Could not label.\tLine: " + line);
                    }else{
                        String label = getLabelFromInstance(labeled.instance(instanceNumber));
                        writer.write(line + ","+label + "\n");
                        instanceNumber ++;
                        System.out.println("labeled as " + label + ".\tLine: " + line);
                    }
                }
            }
            writer.close();
            reader.close();
            missing.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method gets the label from a instance
     * @param instance: A labeled instance.
     * @return : A string containing the label from a instance.
     */
    public String getLabelFromInstance(Instance instance){
        String total = instance.toString();
        String[] elements = total.split(",");
        return elements[6];
    }

    /**
     * This method Calls a R script via the terminal.
     * @param settings: A string with the settings for the R script separated with spaces.
     * @throws IOException: throws exception when encountering a error
     * @throws InterruptedException: throws exception when encountering a error
     */
    private void callR(String[] settings) throws IOException, InterruptedException {
        String  bash = Rscript + " " + settings[0] + " " + settings[1];
        Runtime run = Runtime.getRuntime();
        Process process = run.exec(bash);
        process.waitFor();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line=bufferedReader.readLine()) != null){
            System.out.println(line);
        }
    }

    /**
     *This method classifies given instances with a given model.
     * @param tree: The given CostSensitiveClassifier model.
     * @param toBeTested: The instances that are not classified yet.
     * @throws Exception: throws exception when encountering a error
     */
    private static Instances classifyNewInstance(CostSensitiveClassifier tree, Instances toBeTested) throws Exception{
        Instances labeled = new Instances(toBeTested);
        for (int i = 0; i < toBeTested.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(toBeTested.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        return labeled;
    }

    /**
     * Method that loads the classifier.
     * @return : This method returns a CostSensitiveClassifier model.
     * @throws Exception: throws exception when encountering a error
     */
    private CostSensitiveClassifier loadClassifier() throws Exception {
        return (CostSensitiveClassifier) weka.core.SerializationHelper.read(model);
    }

    /**
     * This method loads a arff datafile.
     * @param datafile: The file name of a arff file to be loaded.
     * @return data: The instances of the arff file.
     * @throws IOException: throws exception when encountering a error
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}
