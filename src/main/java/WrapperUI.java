import org.apache.commons.cli.*;

import java.io.File;
import java.util.Scanner;

public class WrapperUI {

    public static void main(String[] args) {
        // TODO should add comments
        // TODO User should be able to give location for saving of the files

        // setup command line arguments.
        CommandLineParser parser = new BasicParser();
        Options options = new Options();
        options.addOption("f", "file", true, "The file with data to be classified");
        options.addOption("l", "log2", false,
                "Indicates that the file is not log2 transformed yet.");
        options.addOption("i", "instance", true,
                "One singular instance that needs to be classified. should be given as one argument " +
                        "separated with commas. Example 0,81,6.743,-6.743,0.0,30. See readme for order of antibodies");
        options.addOption("o", "output", true, "The location and name of the output file." +
                " Default: Data/predictedValues.csv");
        options.addOption("h", "help", false, "Shows this help");

        String[] input = new String[3];

        try {
            CommandLine commandLine = parser.parse(options, args);
            if (commandLine.hasOption("h")){
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("CommandlineParameters", options);
                return;
            }
            if (commandLine.hasOption("l")) input[1] = "False";
            else input[1] = "True";

            if (commandLine.hasOption("o")) input[2] = commandLine.getOptionValue("o");
            else input[2] = "Data/predictedValues.csv";

            if (commandLine.hasOption("f")) {
                String file = commandLine.getOptionValue("f");
                if (fileExcisits(file)) input[0] = file;
                else {
                    System.out.println("ERROR:\nGiven file: \"" + file + "\" Does not exist");
                    return;
                }
            }else if(commandLine.hasOption("i")){
                Wrapper.createSingleInstanceFile(commandLine.getOptionValue("i"));
                input[0] = "Data/SingleInstance.csv";
            }
            // if no file or instance given ask user for input file.
            else input = userInput();

        }catch (Exception e) {
            e.printStackTrace();
        }
        Wrapper.main(input);
    }

    /**
     * This method checks if the given file exists.
     * @param file: the file to check.
     * @return : a boolean indicating if the file exists.
     */
    private static boolean fileExcisits(String file){
        File myFile = new File(file);
        return myFile.exists();
    }

    /**
     * This method is used to ask the user files and settings.
     * @return input: A string with the user input separated with spaces.
     */
    private static String[] userInput(){
        String[] settings = new String[3];
        Scanner input = new Scanner(System.in);

        boolean isFile = false;
        System.out.println("Please give the file with the data to be classified.");
        while (!isFile) {
            String file = input.nextLine();
            // should test if given file exists
            isFile = fileExcisits(file);
            if (!isFile) System.out.println("Given file does not exist please try again.");
            else settings[0] =  file;
        }

        System.out.println("Is the data log 2 transformed? <No/Yes>");
        boolean isNoOrYes = false;
        while (!isNoOrYes) {
            String log2 = input.nextLine();
            if (log2.toUpperCase().equals("YES")){
                isNoOrYes = true;
                settings[1] = "True";
            } else{
                if (log2.toUpperCase().equals("NO")){
                    isNoOrYes = true;
                    settings[1] = "False";
                }
            }
            if (!isNoOrYes) System.out.println("Please enter No or Yes.");
        }
        settings[2] = "Data/predictedValues.csv";
        return settings;
    }
}
